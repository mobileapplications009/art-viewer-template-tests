package art.viewer.template.test;

import android.test.ActivityInstrumentationTestCase2;
import art.viewer.template.MainActivity;

public class NonNullTests extends
		ActivityInstrumentationTestCase2<MainActivity> {

	MainActivity mainActivity;

	public NonNullTests() {
		super(MainActivity.class);
	}

	public NonNullTests(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();

	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testActivityNotNull() {
		assertNotNull(mainActivity);
	}

	public void testArtViewerNotNull() {
		assertNotNull(mainActivity.artViewer);
	}

	public void testMediaPlayerNotNull() {
		assertNotNull(mainActivity.artViewer.myMediaPlayer);
	}

	public void testImageSwitcherNotNull() {
		assertNotNull(mainActivity.artViewer.imageSwitcher);
	}

}
