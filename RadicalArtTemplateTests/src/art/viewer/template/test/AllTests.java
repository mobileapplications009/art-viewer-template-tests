package art.viewer.template.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	static final int TEST_RUNS = 1;

	/** 
	 * Set number of tests to repeat using RepeatCount, as you add test
	 * classes add them to the test suite. 
	 * 
	 * @return
	 */
	public static Test suite() {
		TestSuite suite = null;
		suite = new TestSuite(AllTests.class.getName());

		// set number of times to run tests repeatedly
		for (int i = 0; i < TEST_RUNS; i++) {
			// $JUnit-BEGIN$
			suite.addTestSuite(NonNullTests.class);
			suite.addTestSuite(MediaPlayerPuzzleTests.class);
			suite.addTestSuite(PageTurnTests.class);
			// $JUnit-END$
		}
		return suite;
	}
}