package art.viewer.template.test;

import android.test.ActivityInstrumentationTestCase2;
import art.viewer.template.MainActivity;

public class PageTurnTests extends
		ActivityInstrumentationTestCase2<MainActivity> {

	MainActivity mainActivity;

	public PageTurnTests() {
		super(MainActivity.class);
	}

	public PageTurnTests(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testRightTurn() {
		final int TURNS = 100;
		final int SPEED = 50;

		int before = mainActivity.commonVariables.currentImagePosition;
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				mainActivity.RightArrowClick(null);
			}
		});
		
		int actual = mainActivity.commonVariables.currentImagePosition;
		assertTrue(actual != before);

		for (int i = 0; i < TURNS; i++) {
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			before = mainActivity.commonVariables.currentImagePosition;
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					mainActivity.RightArrowClick(null);
				}
			});
			actual = mainActivity.commonVariables.currentImagePosition;
			assertTrue(actual != before);
		}
	}

	public void testLeftPageTurns() {
		final int TURNS = 50;
		final int SPEED = 1100;

		int before = mainActivity.commonVariables.currentImagePosition;
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				mainActivity.LeftArrowClick(null);
			}
		});
		
		int actual = mainActivity.commonVariables.currentImagePosition;
		assertTrue(actual != before);

		for (int i = 0; i < TURNS; i++) {
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			before = mainActivity.commonVariables.currentImagePosition;
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					mainActivity.LeftArrowClick(null);
				}
			});
			actual = mainActivity.commonVariables.currentImagePosition;
			assertTrue(actual != before);
		}

	}
}