package art.viewer.template.test;

import engine.MyMediaPlayer;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import art.viewer.template.MainActivity;
import art.viewer.template.R;

/**
 * 
 * MediaPlayer tests to include play and pause functionality as well as toggle.
 * 
 * @author Rick 
 *
 */
public class MediaPlayerPuzzleTests extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mainActivity;
	MediaPlayer mp;

	public MediaPlayerPuzzleTests() {
		super(MainActivity.class);
	}

	public MediaPlayerPuzzleTests(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Log.d("set up", "media player tests");
		mainActivity = getActivity();
		mp = mainActivity.artViewer.myMediaPlayer.mediaPlayer;
	}

	public void testMediaPlayerPlays() {
		if (!mainActivity.commonVariables.playMusic) {
			getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					mainActivity.menu.performIdentifierAction(
							R.id.menu_music_toggle, 0);
				}
			});
			getInstrumentation().waitForIdleSync();
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(mp.isPlaying());
	}

	public void testMediaPlayerMenuPlayingToPauseButtonToggleWorks() {
		getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				mainActivity.menu.performIdentifierAction(
						R.id.menu_music_toggle, 0);
			}
		});
		getInstrumentation().waitForIdleSync();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(mainActivity.artViewer.myMediaPlayer.currentState != MyMediaPlayer.State.Started);
	}

	/**
	 * test headphones unplugged quiets sound
	 */
	public void testHeadphonesUnplugged() {
		if (!mainActivity.commonVariables.playMusic) {
			getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					mainActivity.menu.performIdentifierAction(
							R.id.menu_music_toggle, 0);
				}
			});
			getInstrumentation().waitForIdleSync();
		}

		Intent intent = new Intent(mainActivity.getApplicationContext(),
				MainActivity.class);
		intent.setAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
		mainActivity.myNoisyAudioStreamReceiver.onReceive(
				mainActivity.getBaseContext(), intent);

		float actual = mainActivity.artViewer.myMediaPlayer.currentVolume;
		float expected = .1f;
		assertTrue(actual == expected);
	}

	public void testNotificationReducesSound() {
		// scaffolding flag for SMS to have been received
		mainActivity.artViewer.myMediaPlayer.volumeSet = false;

		if (!mainActivity.commonVariables.playMusic) {
			getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					mainActivity.menu.performIdentifierAction(
							R.id.menu_music_toggle, 0);
				}
			});
			getInstrumentation().waitForIdleSync();
		}
		// create text message
		TelephonyManager telMgr = (TelephonyManager) mainActivity
				.getSystemService(Context.TELEPHONY_SERVICE);
		String phoneNumber = telMgr.getLine1Number();
		// Send SMS
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, "sms body", null, null);

		while (!mainActivity.artViewer.myMediaPlayer.volumeSet) {
		}

		float actual = mainActivity.artViewer.myMediaPlayer.currentVolume;
		float expected = .1f;
		assertTrue(actual == expected);
	}
}